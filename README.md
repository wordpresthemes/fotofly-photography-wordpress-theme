# Fotofly Photography WordPress Theme

The main intention for Fotofly was to create a WordPress Theme for photographers, bloggers and designers that would give a maximum creative freedom for users not deeply involved in design & coding. The result is impressive with its outstanding unique